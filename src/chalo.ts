import { HALO } from "hal-object";

export default class CHALO extends HALO {
  protected _response?: ResponseInit;
  protected _request?: RequestInit;

  public constructor(json: any) {
    console.log(json);
    super(json);
    this.registerRequest(json);
    this.registerResponse(json);
  }

  protected init(json: any) {
    JSON.parse(JSON.stringify(json), (key, value) => {
      if (key === HALO.embeddedProperyName) {
        this._embedded = {};
        for (const rel in value) {
          const related = value[rel];
          if (Array.isArray(related)) {
            this._embedded[rel] = new Array<CHALO>();
            for (let i = 0; i < value[rel].length; i++) {
              (this._embedded[rel] as CHALO[])[i] = new CHALO(value[rel][i]);
            }
          } else {
            this._embedded[rel] = new Array<HALO>(new CHALO(value[rel]));
          }
        }
      }
      return value;
    });
  }

  protected registerResponse(json: any) {
    this._response = json._response;
  }

  protected registerRequest(json: any) {
    this._request = json._request;
  }

  public getResponse(): ResponseInit {
	return this._response || {};
  }

  public getRequest(): RequestInit {
	return this._request || {};
  }
}
