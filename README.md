# Cacheable HAL Object
`cacheable-hal-object` is an experimental extension of [JSON Hypertext Application Language](https://datatracker.ietf.org/doc/html/draft-kelly-json-hal), written in Typescript, for the purpose of consuming JSON from external APIs that supply additional response and request key-value pairs on top of those specified by HAL. It extends the `hal-object` [library](https://gitlab.com/073TruantKernel/hal-object). This library was inspired by HTTP/2's now unsupported `PUSH` frame and the article [The problems with embedding in REST today and how it might be solved with HTTP/2](https://evertpot.com/rest-embedding-hal-http2/) by Evert Pot. The motivation for this library is to allow embedded responses supplied according to the HAL specification to be added to the browser's cache by the inclusion of `_request` and `_response` key-value pairs along side HAL's `_embedded` and `_links`.

This library is experimental and has not been extensively tested. Development on it represented an exploration of media types by its developer and further development on it is currently inactive for the time being. Please use with caution!

# Extensions

#### [tealeaf-object](https://gitlab.com/073TruantKernel/tealeaf-object)
An experimental library that extends `cacheable-hal-object` to add a property for BEM related data.