export const valid = {
  _links: {
    self: {
      href: "http://example.org/api/foo",
    },
    root: {
      href: "http://example.org/api",
    },
    collection: [
      { href: "http://example.org/one" },
      { href: "http://example.org/two" },
    ],
  },
  id: "foo",
  property: "bar",
  _embedded: {
    related: [
      {
        _links: {
          self: {
            href: "http://example.org/api/biz",
          },
        },
        id: "biz",
        property: "baz",
      },
      {
        _links: {
          self: {
            href: "http://example.org/api/qux",
          },
        },
        id: "qux",
        name: "lok",
      },
    ],
    additional: {
      _links: {
        self: {
          href: "http://example.org/api/foobar",
        },
      },
      id: "foobar",
      property: "barfoo",
    },
  },
};

export const noSelf = {
  _links: {
    canonical: {
      href: "http://example.org/api/foo",
    },
  },
};
